import java.util.ArrayList;
public class Customer implements IDisplay {

	int CustomerID;
	String FirstName;
	String LastName;
	String FullName;
	String Email;
	ArrayList<Bill> Listofbills = new ArrayList<Bill>();
	
	public Customer(int CustomerID,String FirstName, String LastName, String FullName, String Email) {
		
			this.CustomerID=CustomerID;
			this.FirstName=FirstName;
			this.LastName=LastName;
			this.FullName=FullName;
			this.Email=Email;
			}
	
	public int getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(int customerID) {
		CustomerID = customerID;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public ArrayList<Bill> getListofbills() {
		return Listofbills;
	}

	public void setListofbills(ArrayList<Bill> listofbills) {
		Listofbills = listofbills;
	}

	
	
	
	public double getTotalAmountToPay() {
				double total = 0;
				
				for (int i = 0; i < Listofbills.size(); i++) {
					total += Listofbills.get(i).getTotalBillAmount();
				}
				return total;
				
			}

	
	
	public void Listofbills(Bill b) {
		Listofbills.add(b);
			
		}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		
		System.out.println("Customer Id: "+this.CustomerID+"\n "+" Customer Full Name: "+this.FullName+"\n "+" Customer EmailId: "+this.Email);
			for (int i = 0; i < Listofbills.size(); i++) {
			Listofbills.get(i).display();
			}
				System.out.println("The Total Amount to Pay:$ "+this.getTotalAmountToPay());
				
				System.out.println("\t****************************************************");
		
		
	}
	

}

