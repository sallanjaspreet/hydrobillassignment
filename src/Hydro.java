import java.util.Date;

public class Hydro extends Bill implements IDisplay {
	
	String AgencyName;
	int UnitConsumed;
	
	public Hydro(int billId, Date billDate, String billType, double totalbillAmount, String agencyName,
						int unitConsumed) {
		super(billId, billDate, billType, totalbillAmount);
					this.AgencyName = agencyName;
				this.UnitConsumed = unitConsumed;
				}

		
	public String getAgencyName() {
		return AgencyName;
	}

	public void setAgencyName(String agencyName) {
		AgencyName = agencyName;
	}	

	public int getUnitConsumed() {
		return UnitConsumed;
	}

	public void setUnitConsumed(int unitConsumed) {
		UnitConsumed = unitConsumed;
	}
	
	@Override
		public void display() {
			// TODO Auto-generated method stub
			super.display();
			System.out.println("Bill Id: " + this.BillId+"\n " + " Bill Date: " + this.BillDate+"\n " + " Bill Type: " + this.BillType+"\n "
					+ " Bill Amount: $" + this.TotalBillAmount+"\n " + " Company Name: " + this.AgencyName +"\n "+ " Units Consumed: " +
					this.UnitConsumed+ "units");
			
			System.out.println("\t****************************************************");
	
		}

}
