import java.util.Date;

public class Bill implements IDisplay {

	int BillId;
	Date BillDate;
	String BillType;
	double TotalBillAmount;
	
	public Bill(int billId, Date billDate, String billType, double totalbillAmount) {
			// TODO Auto-generated constructor stub
		this.BillId=billId;
		this.BillDate=billDate;
		this.BillType=billType;
		this.TotalBillAmount=totalbillAmount;
			}
	
	
	
	public int getBillId() {
		return BillId;
	}



	public void setBillId(int billId) {
		BillId = billId;
	}



	public Date getBillDate() {
		return BillDate;
	}



	public void setBillDate(Date billDate) {
		BillDate = billDate;
	}



	public String getBillType() {
		return BillType;
	}



	public void setBillType(String billType) {
		BillType = billType;
	}



	public double getTotalBillAmount() {
		return TotalBillAmount;
	}



	public void setTotalBillAmount(double totalBillAmount) {
		TotalBillAmount = totalBillAmount;
	}



	public void display()
	{
		
	}
}
