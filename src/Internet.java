import java.util.Date;

public class Internet extends Bill implements IDisplay {
	
	String ProviderName;
	double InternetGbUsed;
	
	
	public Internet(int billId, Date billDate, String billType, double totalbillAmount, String providerName,
						double internetGBused) {
					super(billId, billDate, billType, totalbillAmount);
					// TODO Auto-generated constructor stub
					this.ProviderName = providerName;
					this.InternetGbUsed = internetGBused;
				}


	public String getProviderName() {
		return ProviderName;
	}


	public void setProviderName(String providerName) {
		ProviderName = providerName;
	}


	public double getInternetGbUsed() {
		return InternetGbUsed;
	}


	public void setInternetGbUsed(double internetGbUsed) {
		InternetGbUsed = internetGbUsed;
	}
	
	@Override
		public void display() {
			// TODO Auto-generated method stub
			super.display();
			System.out.println("Bill Id: " + this.BillId + "\n " + " Bill Date: " + this.BillDate + "\n " + " Bill Type: "
					+ this.BillType + "\n " + " Bill Amount: $" + this.TotalBillAmount + "\n " + " Provider Name: "
					+ this.ProviderName + "\n " + " Internet Usage: " + this.InternetGbUsed + "GB");
			
			System.out.println("\t****************************************************");
	
	}

}
