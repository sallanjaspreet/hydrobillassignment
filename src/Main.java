import java.util.Date;

public class Main {

	public static void main(String[] args) {
		
		Customer c1 = new Customer(1, "Peter", "Sigurdson", "Peter Sigurdson", "peter@gmail.com");
				Bill b1 = new Hydro(1, new Date(), "Hydro", 45.35, "HydroOne", 29);
				c1.Listofbills(b1);
				Bill b2 = new Internet(2, new Date(), "Internet", 56.50, "Rogers", 500);
				c1.Listofbills(b2);
				c1.display();
		
				Customer c2 = new Customer(2, "Emad", "Nasrallah", "Emad Nasrallah", "dr.emad@gmail.com");
				Bill b3 = new Hydro(1, new Date(), "Hydro", 45.35, "Enbridge", 29);
				c2.Listofbills(b3);
				Bill b4 = new Internet(2, new Date(), "Internet", 56.50, "Rogers", 500);
				c2.Listofbills(b4);
				Bill b5 = new Mobile(3,new Date(), "Mobile", 250.69, "Galaxy Samsung Inc.",
						"Prepaid Talk + Text plan", 12345678, 5, 356);
				c2.Listofbills(b5);
				Bill b6 = new Mobile(4, new Date(), "Mobile", 300.78, "Apple Inc. iPhone X MAX+",
						"LTE+3G 9.5GB Promo Plan", 5678947, 4, 230);
				c2.Listofbills(b6);
				c2.display();
				Customer c3 = new Customer(3, "Mohammad", "Kiani", "Muhammad Kiani", "mkiani@gmail.com");
				Bill b7 = new Mobile(4, new Date(), "Mobile", 300.78, "Apple Inc. iPhone X MAX+",
						"LTE+3G 9.5GB Promo Plan", 5678947, 4, 230);
				c2.Listofbills(b7);

	}

}
